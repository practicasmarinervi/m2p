<!DOCTYPE html>
<!--
Recoge todos los datos del formulario utilizando la superglobal $_REQUEST
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>p3_4</title>
    </head>
    <body>
        <table width="100%" border="1">
            <tr>
                <?php
                foreach ($_REQUEST as $indice => $valor) {
                    if ($indice == "enviar") {
                        continue;
                        //Este elemento continue permite hacer que cuando el elemento a mostrar sea el
                        //boton, el foreach continue realizando el siguiente ciclo
                    }
                    echo "<th>$indice</th>";
                }
                ?>
            </tr>
            <tr>
                <?php
                foreach ($_REQUEST as $indice => $valor) {
                    if ($indice == "enviar") {
                        continue;
                    }
                    echo "<td>$valor</td>";
                }
                ?>  
            </tr>
        </table>
    </body>
</html>
