<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>p3_5</title>
    </head>
    <body>
        <?php
        // Disponible desde PHP 4.1.0
        
        echo $_POST['username'];
        echo $_REQUEST['username'];
        
        //Ya no esta disponible desde PHP6. A partir de PHP 5.0.0 estas variables predefinidas
        //largas pueden ser deshabilitadas con la directiva register_long_arrays
        
        echo $HTTP_POST_VARS['username'];
        
        //Disponible si la directiva de PHP register_globals=on. A partir
        //de PHP 4.2.0 el valor predeterminado de register_globals=off.
        //Usar o depender de este metodo no es recomendable.
        
        echo $username;
        
        //Disponible desde PHP 4.1.0
        //Con esta funcion del lenguaje podemos importar las variables POST con el prefijo p_
        
        //import_request_variabless('p', 'p_');
        //Advertencia: Esta función ha sido declarada OBSOLETA desde PHP 5.3.0 y
        // ELIMINADA a partir de PHP 5.4.0.
        echo $p_username;
        
        /* Mostrar el correo electronico*/
        
        echo $_POST['email'];
        echo $_REQUEST['email'];
        echo $p_email;//No fucionaa y se ha sustituido por extract()
        ?>
    </body>
</html>
