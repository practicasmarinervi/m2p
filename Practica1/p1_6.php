<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>p1_6</title>
    </head>
    <body>
        <?php
        //con la funcion echo se pueden utilizar como operadores de
        //concatenacion el "." o la "," (con print solamente el punto)
        echo 'Quiero que coloque este texto en pantalla' . '<p align="center">';
        echo 'Academia Alpe' , '</p>';
       
        
        
        /* Lo modificamos para que nada mas aparezca una funcion echo y sin
         * operadores de concatenacion
         */
        echo 'Quiero que coloque este texto en pantalla <p align="center">Academia Alpe</p>';
        ?>
        
        
    </body>
</html>
