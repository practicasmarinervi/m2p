<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>p1_1</title>
    </head>
    <body>
        En este ejercicio vamos a escribir un texto en la pagina web a traves de PHP
        <hr>
        <?php
        // Esto es un comentario
        echo "Este texto esta escrito desde el script de PHP.";
        /*Esto es otro comentario
         pero con la diferencia de que es de varias lineas
        */
        echo "Este texto tambien se escribe desde el script de php.";
        ?>
        <hr>
        Esto esta escrito en HTML normal
        <hr>
        <?php
        #######################################################
        ######### Este comentario es de una sola linea ########
        #######################################################
        
        # En una pagina podemos colocar tantos scripts de php como se desee
        
        print ("Esta es otra forma de escribir cosas en la web");
        
        ?>
        
    </body>
</html>
