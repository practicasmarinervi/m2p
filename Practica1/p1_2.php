<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>p1_2</title>
    </head>
    <body>
        <table width='100%' border='1'>
            <tr>
                <td>
                    <?php
                    //la instruccion echo admite comillas dobles o simples
                    echo 'Este texto quiero que lo escribas utilizando la funcion echo de php';
                    ?>
                </td>
                <td>Aqui debe colocar un texto directamente en HTML  
                </td>
            </tr>
            <tr>
                <td>
                    <?php
                    //con la instruccion print se pueden utilizar coillas dobles o simples
                    print 'Este texto quiero que lo escribas utilizando la funcion print de php';
                    ?>
                </td>
                <td>
                    <?php
                    echo 'Academia Alpe';
                    ?>
                </td>
            </tr>
        </table>
    </body>
</html>
