<?php

/*
 * Modelar rectangulos por medio de cuatro puntos (los vértices)
 * Un constructor dibuja crea el rectangulo a partir de los 4 vértices
 * Otro que lo crea partiendo de la base y la altura
 * Incluye un metodo para calcular superficie y otro que lo desplac en el plano y
 * ademas uno que dibuje un rectángulo
 */

class Rectangulo {

    private $x;
    private $y;
    private $base;
    private $altura;
    private $superficie;

    public function __construct() {
        $argumentos = func_get_args();
        $numeroArgumentos = func_num_args();
        /* Inicializo */
        $this->setX(0);        
        $this->setY(0);
        $this->setBase(0);
        $this->setAltura(0);
        /* asigno por llamada */
        if (method_exists($this, $f = 'constructor' . $numeroArgumentos)) {
            call_user_func_array(array($this, $f), $argumentos);
        }
    }
    
    public function constructor4($x,$y,$base, $altura) {
        $this->setX($x);
        $this->setY($y);
        $this->setBase($base);
        $this->setAltura($altura);
        $this->dibujar($this->getX(),$this->getY(),$this->getBase(),$this->getAltura());
    }
    

    public function constructor2($base, $altura) {
        $this->setBase($base);
        $this->setAltura($altura);
        $this->dibujar($this->getBase(),$this->getAltura());
    }

    
    public function getX() {
        return $this->x;
    }

    public function getY() {
        return $this->y;
    }

    public function getBase() {
        return $this->base;
    }

    public function getAltura() {
        return $this->altura;
    }

    public function getSuperficie() {
        return $this->superficie;
    }

    public function setX($x) {
        $this->x = $x;
        return $this;
    }

    public function setY($y) {
        $this->y = $y;
        return $this;
    }

    public function setBase($base) {
        $this->base = $base;
        return $this;
    }

    public function setAltura($altura) {
        $this->altura = $altura;
        return $this;
    }

    public function setSuperficie($superficie) {
        $this->superficie = $superficie;
        return $this;
    }
              
    public function superficie(){
       
     $this->setSuperficie($this->getBase()*$this->getAltura());
        return $this->getSuperficie();
    }

   private function dibujar() {
        $argumentos = func_num_args();
        if ($argumentos == 2) {
            echo'<rect x="0" y="0" width="'.func_get_arg(0).'" height="'.func_get_arg(1).'" fill="red"/>';
        } else if ($argumentos == 4) {
            echo'<rect x="'.func_get_arg(0).'" y="'.func_get_arg(1).'" width="'.func_get_arg(2).'" height="'.func_get_arg(3).'" fill="red"/>';
        }
    }
    
    public function desplazar(){
        $this->setX(rand(0,600));
        $this->setY(rand(0,300));
        $this->dibujar($this->getX(),$this->getY(),$this->getBase(),$this->getAltura());
    }

}
