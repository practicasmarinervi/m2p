 <?php
       spl_autoload_register(function ($nombre_clase) {
    include 'Clases/' . $nombre_clase . '.php';
});
        ?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio2</title>
    </head>
    <body>
        <?php
        $p=new Punto(10,10);
        $p2=new Punto(50,50);
        $l=new Linea($p,$p2);
        var_dump($l);
        $l->render();
        
        $p3=new Punto(2,4);
        $p4=new Punto(8,16);
        $linea=new Linea($p3,$p4);
        $linea->render();
        $linea->mueveDerecha(4);
        $linea->mueveArriba(2);
        $linea->render();
        ?>
    </body>
</html>
