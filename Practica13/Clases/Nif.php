<?php

/*
 * Clase que se utilizara para mantener DNIs con su correspondiente letra
 */

/**
 * Description of Nif
 *Atributos: Numero de DNI(entero largo) y la letra que le corresponde
 * metodo predeterminado constructor que inicializa nº a 0 y letra espacio blanco(Nif no valido)
 * metodo que recibe dni y establece letra correspondiente
 * @author anuski
 */
class Nif {
    private $dni;
    private $dniLetra;
    
    public function __construct($dni=0, $letra="") {
       if($dni != 0) {
        $this->dni = $dni;
        $this->dniLetra = $letra;
        $this->calcular();
       }
       else {
           echo 'NIF no valido!!';
       }
        
    }
    
     public function setValor ($dniNuevo) {
        $this->dni = $dniNuevo;
        $this->calcular();
    }
    
    public function getValor () {
        echo $this->dni;
    }
   
 
    private function calcular() {
        $letra = 'TRWAGMYFPDXBNJZSQVHLCKE';
        //Calculo el resto del DNI
        $resto = $this->dni% 23;
        
        //Concateno la letra con el DNI
        for($i = 0; $i < strlen($letra); $i++) {
            if($resto == $i) {
                $this->dni .= '-'.$letra[$i].'<br />';
                break;
            }
        }
    }
    
}

 

