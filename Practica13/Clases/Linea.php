<?php

/*
 * Clase Linea con atributos _puntoA y _puntoB por donde pasa la linea
 *  * 
 */


class Linea {
    public $puntoA;
    public $puntoB;
    public $linea0;
    
    public function __construct() {
        $argumentos = func_get_args();
        $numeroArgumentos = func_num_args();
        /* Inicializo */
        $this->setPuntoA('0.0');
        $this->setPuntoB('0.0');
        $this->setLinea0($this->getPuntoA(). "," .$this->getPuntoB());
        /* asigno por llamada */
        if (method_exists($this, $f = 'constructor' . $numeroArgumentos)) {
            call_user_func_array(array($this, $f), $argumentos);
        }
    }

    public function constructor2($p1,$p2){
        
        $this->setPuntoA($p1);
        $this->setPuntoB($p2);
    }
    
    public function getPuntoA() {
        return $this->puntoA;
    }

    public function getPuntoB() {
        return $this->puntoB;
    }

    public function setPuntoA($puntoA) {
        $this->puntoA = $puntoA;
        return $this;
    }

    public function setPuntoB($puntoB) {
        $this->puntoB = $puntoB;
        return $this;
    }
    
    public function getLinea0() {
        return $this->linea0;
    }

    public function setLinea0($linea0) {
        $this->linea0 = $linea0;
        return $this;
    }
    
    public function mueveDerecha($distancia){
        $this->puntoA->setX(+$distancia);
    }
    
     public function mueveIzquierda($distancia){
        $this->puntoA->setX(-$distancia);
    }
    
     public function mueveArriba($distancia){
        $this->puntoB->setY(-$distancia);
    }
    
     public function mueveAbajo($distancia){
        $this->puntoB->setY(+$distancia);
    }

    
    public function render(){
        
        echo "<div>";
        
        echo "[(". $this->getLinea0() .")(" . $this->getPuntoA() . "," . $this->getPuntoB() . ")]";
        
        
        echo "</div>";
    }
   
}
