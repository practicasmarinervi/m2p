<?php

/*
 *Clase Punto con dos miembros privados X e Y
 */


class Punto {
    private $x;
    private $y;
    
    public function __construct($x, $y) {
        $this->setX($x);
        $this->setY($y);
    }

    public function getX() {
        return $this->x;
    }

    public function getY() {
        return $this->y;
    }

    public function setX($x) {
        $this->x = $x;
        return $this;
    }

    public function setY($y) {
        $this->y = $y;
        return $this;
    }
    public function __toString() {
        return $this->getX(). "." . $this->getY();
    }
}
