<!DOCTYPE html>
<!--
ARRAY con dias de la semana
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>p2_4</title>
    </head>
    <body>
        <?php
       $dias[0]="Lunes";
       $dias[1]="Martes";
       $dias[2]="Miercoles";
       $dias[3]="Jueves";
       $dias[4]="Viernes";
       $dias[5]="Sabado";
       $dias[6]="Domingo";
       echo $dias[0] , $dias[4];
 /*Este array es de tipo numerico. Podemos crearle directamente sin colocar
  * los numers ya que php coloca automaticamente el nuevo elemento del array
  */
       $dias[]="Lunes";
       $dias[]="Martes";
       $dias[]="Miercoles";
       $dias[]="Jueves";
       $dias[]="Viernes";
       $dias[]="Sabado";
       $dias[]="Domingo";
       echo $dias[0] , $dias[4];
       
       
        ?>
    </body>
</html>
