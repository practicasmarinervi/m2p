<!DOCTYPE html>
<!--
Arrays escalares
-->
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>p2_6</title>
        <style type="text/css" media="screen">
            #layer1{
                position: absolute;
                left: 90px;
                top: 115px;
                width: 323px;
                height: 504px;
                z-index: 1;
                padding: 10px;
                border: medium solid #0000ff;
            }	
            #layer2{
                position: absolute;
                left: 519px;
                top: 170px;
                width: 503px;
                height: 266px;
                z-index: 2;
            }	
        </style>
    </head>
    <body>
    <body>
        <?php
        $dias_semana = array("Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo");
        $colores = array("Rojo", "Verde", "Azul");
        ?>
        <div id="layer1">
            <p>Los dias de la semana son:</p>
            <?php
            echo "<p>$dias_semana[0]</p>";
            echo "<p>$dias_semana[1]</p>";
            echo "<p>$dias_semana[2]</p>";
            echo "<p>$dias_semana[3]</p>";
            echo "<p>$dias_semana[4]</p>";
            echo "<p>$dias_semana[5]</p>";
            echo "<p>$dias_semana[6]</p>";
            ?>
        </div>

        <div id="layer2">   
            <p>Los colores son:</p>
            <?php
            echo "<p>$colores[0]</p>";
            echo "<p>$colores[1]</p>";
            echo "<p>$colores[2]</p>";
            ?>
        </div> 
    </body>
</html>
