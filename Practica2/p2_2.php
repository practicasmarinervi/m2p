<!DOCTYPE html>
<!--
USO DE VARIABLES
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>p2_2</title>
    </head>
    <body>
        <?php
        // Creamos dos variables
        $cadena1='Pasa';
        $cadena2='tiempos';
        //Las unimos sin necesidad de una tercera variable
        echo '<p>' , $cadena1 , $cadena2 , '</p>';
        
        
        /* Lo modificamos para no tener que utilizar el operador de concatenacion
         * CON las comillas dobles se puede colocar el nombre de las variables directamente
         */
        $cadena1='Pasa';
        $cadena2='tiempos';
        echo "<p>$cadena1$cadena2</p>";
        
        /*Si hubiesemos puesto las comillas simples
         * escribiria textualmente $cadena1$cadena2
         */
        ?>
        
        
        
        
        
    </body>
</html>
